package org.winecollection.winecollection.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.winecollection.winecollection.controllers.dto.request.WineEntryRequest;
import org.winecollection.winecollection.exceptions.wine.WineAlreadyExistsException;
import org.winecollection.winecollection.exceptions.wine.WineNotFoundException;
import org.winecollection.winecollection.models.Review;
import org.winecollection.winecollection.models.Wine;
import org.winecollection.winecollection.services.ReviewService;
import org.winecollection.winecollection.services.WineService;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
@ExtendWith(MockitoExtension.class)
public class WineControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WineService wineService;

    @MockBean
    private ReviewService reviewService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void createWine_thenReturnCreated() throws Exception {
        WineEntryRequest request = new WineEntryRequest("Amarena", "Pinot Noir", "Bologna", 2017);
        Wine wine = new Wine("Amarena", "Pinot Noir", "Bologna", 2017);

        when(wineService.createWine(any(WineEntryRequest.class))).thenReturn(wine);

        mockMvc.perform(post("/api/v1/wines")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value(request.name()));
    }

    @Test
    void createWine_whenWineAlreadyExists_thenReturnConflict() throws Exception {
        WineEntryRequest request = new WineEntryRequest("Amarena", "Pinot Noir", "Bologna", 2017);
        when(wineService.createWine(any(WineEntryRequest.class))).thenThrow(WineAlreadyExistsException.class);

        mockMvc.perform(post("/api/v1/wines")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isConflict());
    }

    @Test
    void shouldReturnWineById() throws Exception {
        Wine wine = new Wine("Amarena");
        when(wineService.getWineById(1L)).thenReturn(wine);

        mockMvc.perform(get("/api/v1/wines/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Amarena"));
    }

    @Test
    void deleteWineById_thenReturnOk() throws Exception {
        mockMvc.perform(delete("/api/v1/wines/1"))
                .andExpect(status().isOk());

        verify(wineService, times(1)).deleteWineById(1L);
    }

    @Test
    void deleteWineById_whenWineNotFound_thenReturnNotFound() throws Exception {
        doThrow(new WineNotFoundException(1L)).when(wineService).deleteWineById(anyLong());

        mockMvc.perform(delete("/api/v1/wines/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturnAllWines_andOk() throws Exception {
        Wine wine = new Wine("Amarena");
        when(wineService.getAllWines()).thenReturn(Collections.singletonList(wine));

        mockMvc.perform(get("/api/v1/wines"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value(wine.getName()));
    }

    @Test
    void shouldReturnWineReview_andOk() throws Exception {
        Review review = new Review("Loved it. Would buy again!", 5);
        when(reviewService.getAllWineReviews(1L)).thenReturn(Collections.singletonList(review));

        mockMvc.perform(get("/api/v1/wines/1/reviews"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].content").value("Loved it. Would buy again!"))
                .andExpect(jsonPath("$[0].rating").value(5));
    }

    @Test
    void getWineReview_whenWineNotFound_thenReturnNotFound() throws Exception {
        when(reviewService.getAllWineReviews(1L)).thenThrow(WineNotFoundException.class);

        mockMvc.perform(get("/api/v1/wines/1/reviews"))
                .andExpect(status().isNotFound());
    }
}
