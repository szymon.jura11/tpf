-- Populate the users table with more users
INSERT INTO users (username, password) VALUES
                                           ('john_doe', 'pass123'),
                                           ('mary_jane', 'pass234'),
                                           ('alice_wonder', 'pass345'),
                                           ('bob_stone', 'pass456'),
                                           ('carol_keys', 'pass567');

-- Populate the wines table with more wines
INSERT INTO wines (name, varietal, region, vintage) VALUES
                                                        ('Chardonnay', 'Chardonnay', 'California', 2017),
                                                        ('Merlot', 'Merlot', 'Bordeaux', 2015),
                                                        ('Cabernet Sauvignon', 'Cabernet Sauvignon', 'Napa Valley', 2016),
                                                        ('Pinot Noir', 'Pinot Noir', 'Burgundy', 2018),
                                                        ('Sauvignon Blanc', 'Sauvignon Blanc', 'Loire Valley', 2019),
                                                        ('Syrah', 'Syrah', 'Rhône', 2014),
                                                        ('Riesling', 'Riesling', 'Mosel', 2015),
                                                        ('Zinfandel', 'Zinfandel', 'Sonoma', 2016);

-- Populate the wineries table with more wineries
INSERT INTO wineries (name, location) VALUES
                                          ('Silver Oak', 'Napa Valley'),
                                          ('Chateau Ste. Michelle', 'Washington State'),
                                          ('Penfolds', 'South Australia'),
                                          ('Domaine de la Romanée-Conti', 'Burgundy'),
                                          ('Antinori', 'Tuscany'),
                                          ('Vega Sicilia', 'Ribera del Duero'),
                                          ('Cloudy Bay', 'Marlborough');

-- Populate the reviews table with more reviews, assuming random foreign key relation
-- Below values for user_id and wine_id should be adjusted according to actual records in "users" and "wines" tables
INSERT INTO reviews (user_id, wine_id, content, rating) VALUES
                                                            (1, 1, 'Tastes amazing, with perfect balance!', 5),
                                                            (1, 2, 'Loved it. Would buy again!', 4),
                                                            (2, 1, 'Too oaky for my taste.', 3),
                                                            (3, 3, 'An exceptional experience!', 5),
                                                            (4, 2, 'Decent, but not impressive.', 3),
                                                            (5, 4, 'The aroma is captivating!', 5),
                                                            (2, 4, 'Very smooth and well-structured.', 4);

-- Populate the collections table with more entries
-- Below values for user_id and wine_id should be adjusted according to actual records in "users" and "wines" tables
INSERT INTO collections (user_id, wine_id, added_date) VALUES
                                                           (1, 1, '2023-01-15'),
                                                           (2, 2, '2023-02-10'),
                                                           (3, 3, '2023-02-17'),
                                                           (4, 4, '2023-03-08'),
                                                           (5, 5, '2023-01-22');

-- Populate the tastings table with more tastings
INSERT INTO tastings (location, tasting_date) VALUES
                                                  ('Private Cellar Tasting Room', '2023-05-10 19:00:00'),
                                                  ('Annual Wine Expo', '2023-06-22 17:00:00'),
                                                  ('Local Vineyard Tour', '2023-07-11 12:00:00'),
                                                  ('Wine & Cheese Event', '2023-08-05 18:00:00');

-- Populate the tasting_wines table with more wine-tasting associations, assuming tastings id's 1-4
-- and wines id's 1-8
INSERT INTO tasting_wines (tasting_id, wine_id) VALUES
                                                    (1, 1),
                                                    (1, 2),
                                                    (2, 3),
                                                    (2, 4),
                                                    (3, 5),
                                                    (3, 6),
                                                    (4, 7),
                                                    (4, 8);