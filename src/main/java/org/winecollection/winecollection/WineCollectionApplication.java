package org.winecollection.winecollection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WineCollectionApplication {

    public static void main(String[] args) {
        SpringApplication.run(WineCollectionApplication.class, args);
    }

}
