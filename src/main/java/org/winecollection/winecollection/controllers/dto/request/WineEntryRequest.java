package org.winecollection.winecollection.controllers.dto.request;

public record WineEntryRequest(String name,
                               String varietal,
                               String region,
                               int vintage) {
}
