package org.winecollection.winecollection.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.winecollection.winecollection.controllers.dto.request.WineEntryRequest;
import org.winecollection.winecollection.models.Review;
import org.winecollection.winecollection.models.Wine;
import org.winecollection.winecollection.services.ReviewService;
import org.winecollection.winecollection.services.WineService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/wines")
public class WineController {
    private final WineService wineService;
    private final ReviewService reviewService;

    @GetMapping
    public ResponseEntity<List<Wine>> getAllWines() {
        List<Wine> wines = wineService.getAllWines();

        return ResponseEntity.ok(wines);
    }

    @PostMapping
    public ResponseEntity<Wine> createWine(@RequestBody WineEntryRequest request) {
        Wine wine = wineService.createWine(request);

        return new ResponseEntity<>(wine, HttpStatus.CREATED);
    }

    @GetMapping("/{wineId}")
    public ResponseEntity<Wine> getWineById(@PathVariable Long wineId) {
        Wine wine = wineService.getWineById(wineId);

        return ResponseEntity.ok(wine);
    }
    @DeleteMapping("/{wineId}")
    public ResponseEntity<Wine> deleteWine(@PathVariable Long wineId) {
        wineService.deleteWineById(wineId);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{wineId}/reviews")
    public ResponseEntity<List<Review>> getWineReview(@PathVariable Long wineId) {
        List<Review> wineReviews = reviewService.getAllWineReviews(wineId);

        return ResponseEntity.ok(wineReviews);
    }
}