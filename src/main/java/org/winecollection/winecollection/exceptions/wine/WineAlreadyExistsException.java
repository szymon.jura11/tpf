package org.winecollection.winecollection.exceptions.wine;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class WineAlreadyExistsException extends RuntimeException {
    public WineAlreadyExistsException(String name) {
        super("Wine with name %s already exists".formatted(name));
    }
}
