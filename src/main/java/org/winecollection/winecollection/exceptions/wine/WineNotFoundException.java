package org.winecollection.winecollection.exceptions.wine;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class WineNotFoundException extends RuntimeException {
    public WineNotFoundException(Long id) {
        super("Wine with id %d not found".formatted(id));
    }
}
