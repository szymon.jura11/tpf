package org.winecollection.winecollection.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.winecollection.winecollection.models.Winery;

public interface WineryRepository extends JpaRepository<Winery, Long> {
}
