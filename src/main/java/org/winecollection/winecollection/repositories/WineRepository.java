package org.winecollection.winecollection.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.winecollection.winecollection.models.Wine;

import java.util.Optional;

public interface WineRepository extends JpaRepository<Wine, Long> {

    Optional<Wine> findByName(String name);
}
