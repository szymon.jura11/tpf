package org.winecollection.winecollection.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.winecollection.winecollection.models.Tasting;

public interface TastingRepository extends JpaRepository<Tasting, Long> {
}
