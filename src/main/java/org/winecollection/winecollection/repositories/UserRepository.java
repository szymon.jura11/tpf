package org.winecollection.winecollection.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.winecollection.winecollection.models.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
