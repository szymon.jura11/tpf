package org.winecollection.winecollection.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "tastings")
public class Tasting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String location;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date tastingDate;

    @ManyToMany
    @JoinTable(
            name = "tasting_wines",
            joinColumns = @JoinColumn(name = "tasting_id"),
            inverseJoinColumns = @JoinColumn(name = "wine_id")
    )
    private Set<Wine> wines;

}
