package org.winecollection.winecollection.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "wines")
public class Wine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column
    private String varietal;

    @Column
    private String region;

    @Column
    private Integer vintage;

    @JsonManagedReference
    @OneToMany(mappedBy = "wine")
    private List<Review> reviews;

    public Wine(String name) {
        this.name = name;
    }

    public Wine(String name, String varietal, String region, Integer vintage) {
        this.name = name;
        this.varietal = varietal;
        this.region = region;
        this.vintage = vintage;
    }
}
