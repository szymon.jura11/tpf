package org.winecollection.winecollection.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.winecollection.winecollection.controllers.dto.request.WineEntryRequest;
import org.winecollection.winecollection.exceptions.wine.WineAlreadyExistsException;
import org.winecollection.winecollection.exceptions.wine.WineNotFoundException;
import org.winecollection.winecollection.models.Wine;
import org.winecollection.winecollection.repositories.WineRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WineService {
    private final WineRepository wineRepository;

    public List<Wine> getAllWines() {
        return wineRepository.findAll();
    }

    public Wine getWineById(Long id) {
        return wineRepository.findById(id)
                .orElseThrow(() -> new WineNotFoundException(id));
    }

    public Wine createWine(WineEntryRequest request) {
        if (wineRepository.findByName(request.name()).isPresent())
            throw new WineAlreadyExistsException(request.name());

        Wine wine = new Wine(request.name(), request.varietal(), request.region(), request.vintage());

        return wineRepository.save(wine);
    }

    public void deleteWineById(Long id) {
        Wine wine = getWineById(id);
        wineRepository.delete(wine);
    }
}
