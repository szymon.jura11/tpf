package org.winecollection.winecollection.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.winecollection.winecollection.models.Winery;
import org.winecollection.winecollection.repositories.WineryRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WineryService {
    private WineryRepository wineryRepository;

    public List<Winery> getAllWineries() {
        return wineryRepository.findAll();
    }

    public Winery getWineryById(Long id) {
        return wineryRepository.findById(id).orElseThrow();
    }
}
