package org.winecollection.winecollection.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.winecollection.winecollection.models.Tasting;
import org.winecollection.winecollection.repositories.TastingRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TastingService {
    private final TastingRepository tastingRepository;

    public List<Tasting> getAllTastings() {
        return tastingRepository.findAll();
    }

    public Tasting getTastingById(Long id) {
        return tastingRepository.findById(id).orElseThrow();
    }
}
