package org.winecollection.winecollection.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.winecollection.winecollection.models.Review;
import org.winecollection.winecollection.repositories.WineRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ReviewService {
    private final WineService wineService;

    public List<Review> getAllWineReviews(Long wineId) {
        return wineService.getWineById(wineId).getReviews();
    }
}
