package org.winecollection.winecollection.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.winecollection.winecollection.models.WineCollection;
import org.winecollection.winecollection.repositories.UserRepository;

@Service
@RequiredArgsConstructor
public class WineCollectionService {
    private final UserRepository userRepository;

    public WineCollection getUserWineCollection(Long userId) {
        return userRepository.findById(userId).orElseThrow().getCollection();
    }

}
